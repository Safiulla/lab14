#include "date.h"
#include "date.cpp"

#include <iostream>
#include <string>

int main(){
	Date d(25, 3, 2020);
	Date a(30, 2, 2018);
	
	std::cout << d << std::endl;
	std::cout << a << std::endl;
	if (!(d < a))
		std::cout << "True" << std::endl;
}