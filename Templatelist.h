#ifndef _LIST_H_
#define _LIST_H_

#include <iostream>
#include <ostream>

template<typename T, int maxSize>
class list {
	private:
		T stuff[maxSize];
		int tempIndex = 0;
	public:
		list() {}
	
	// Methods defined here because templates do wacky stuff : Source Stackoverflow
		void add(T item){
			this->stuff[this->tempIndex] = item;
			this->tempIndex++;
		}
		
		T get(int index){
			return this->stuff[index];
		}
		
		void remove(int index){
			for(int i=(index+1); i<maxSize; i++)
        this->stuff[i-1] = this->stuff[i];
			tempIndex -= 1;
		}
		
		int size(){
			return (this->tempIndex);
		}
		
		T& operator[](int index){
			return this->stuff[index];
		}
		
		friend std::ostream& operator<<(std::ostream& cOut, list<T, maxSize> stuff){
			cOut << "[ ";
			for(int i = 0; i< stuff.size(); i++)
				cOut << stuff.stuff[i] << " ";
			cOut << "]";
			return cOut;
		}
};
#endif