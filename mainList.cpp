#include "Templatelist.h"
#include <string>
#include <iostream>

int main() {
	list<int, 10> stuff;
	std::cout << "List size: " << stuff.size() <<std::endl;
	
	stuff.add(2);
	stuff.add(4);
	stuff.add(7);
	stuff.add(1);
	
	int temp = stuff[1];
	std::cout << temp << std::endl;
	
	std::cout << stuff << std::endl;
	
	stuff.remove(1);
	temp = stuff[1];
	std::cout << temp << std::endl;
	std::cout << stuff << std::endl;
	
	std::cout << "Some square numbers:\n";
  for (int i = 0; i < stuff.size (); ++i)
		std :: cout << stuff[i] << " " ;
}

/*
list<int, 10> stuff;
	
	std::cout << "List size: " << stuff.size() <<std::endl;
	
	stuff.add(2);
	
	stuff.add(4);
	
	stuff.add(7);
	
	stuff.add(1);
	
	std::cout << "List size: " << stuff.size() <<std::endl;
	
	std::cout << stuff;
	
	stuff.remove(2);
	
	std::cout << "\n";
	
	for(int i=0; i<< stuff.size(); i++){
		int temp = stuff[i];
		std::cout << temp << " " << std::endl;
	}
	
	std::cout << "\nList size after reducing: " << stuff.size() <<std::endl;
*/