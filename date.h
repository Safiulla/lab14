#ifndef _DATE_H_
#define _DATE_H_

#include <iostream>
#include <ostream>

class Date{

private:
  int day;
  int month;
  int year;

public:
  Date(int day, int month, int year);
  int getDay();
  int getMonth();
  int getYear();
  friend bool operator <(const Date& first, const Date& second);
  friend std::ostream& operator<<(std::ostream& out, Date date);
};

#endif
